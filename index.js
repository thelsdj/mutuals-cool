var config = require('_/config');
var app = require('_/app');
var log = require('_/log');

app.listen(config.PORT, function () {
    log.info('app listening on port', config.PORT);
});
