var webdriver = require('selenium-webdriver');
var phantom = require('phantomjs2-server');
var app = require('_/app');

var chai = require('chai');

var chaiWebdriver = require('chai-webdriver');

const By = webdriver.By;

var driver;

describe('Mutuals? Cool! Index', function() {
    this.timeout(10000);

    before(function (done) {
        phantom.start().then(function() {
            driver = new webdriver.Builder().
                usingServer(phantom.address()).
                withCapabilities({ "browserName": "phantomjs" }).
                build();
        }).then(function() {
            chai.use(chaiWebdriver(driver));
            app.listen(3001, function() {
                done();
            });
        });
    });

    beforeEach(function() {
        return driver.navigate().to('http://localhost:3001/');
    });

/*  As a visitor, I would like to see a description of Mutuals? Cool!
    so I can know if it is something I would like to use. */
    it('displays a description of the service', function() {
        return chai.expect('#mutuals-description').dom.to.be.visible();
    });
});
