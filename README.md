# Mutuals, Cool!

This is the code for the [Mutuals, Cool!](https://mutuals.cool/) website.

The website is (going to be) similar to [Curious Cat](https://curiouscat.me/), but limited to only mutual follows on twitter. Only people who you follow and who follow you can ask questions or leave comments. Will probably be optional whether your page of answers will be open to public view (but definitely off by default).

The website is written in [Node.js](http://nodejs.org/) using [Express](https://expressjs.com/) framework. Probably also needs [Heroku](http://heroku.com/) to run, though could be hacked to run elsewhere. I will make note of things I know that are Heroku dependent.

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) installed.

You'll need a [Twitter App](https://apps.twitter.com/). Currently only needs
read-only permission. Leave callback URL unlocked for local dev testing.

You will need an SSL certificate for production use.

You will need a local [Redis](http://redis.io/) server running. Default port of
6379 is where code looks for it by default.

```sh
$ git clone git@gitlab.com:thelsdj/mutuals-cool.git # or clone your own fork
$ cd mutuals-cool
$ npm install
$ cp example.env .env
$ export CONSUMER_KEY=your_twitter_app_consumer_key
$ export CONSUMER_SECRET=your_twitter_app_consumer_secret
$ bash scripts/twitter-bearer-token.sh
$ $EDITOR .env # fill out twitter key/secret/token and pick a SESSION_SECRET
$ npm start
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

Go to [/twitter/connect](http://localhost:5000/twitter/connect) to see current functionality.

## Deploying to Heroku

```
$ heroku create
$ git push heroku master
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Documentation

For more information about using Node.js on Heroku, see these Dev Center articles:

- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
- [Best Practices for Node.js Development](https://devcenter.heroku.com/articles/node-best-practices)
- [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)
