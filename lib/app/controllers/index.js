exports.index = function (request, response) {
    response.render('pages/index');
};

exports.screen_name = function (request, response) {
    response.render('pages/welcome',
        { screen_name: request.params.screen_name });
};
