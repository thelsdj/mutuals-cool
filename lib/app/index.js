/*jslint node: true, es5: true */
"use strict";

var express = require('express');
var app = express();
var session = require('express-session');
var enforce = require('express-sslify');
var helmet = require('helmet');
var config = require('_/config');
var redis = require('_/redis')(session);

app.use(express.static(__dirname + '/../../public'));
app.use(session({
    store: redis.getStore(),
    secret: config.SESSION_SECRET,
    saveUninitialized: false,
    resave: true
}));
app.use(helmet());

if (config.env === 'production') {
    app.use(enforce.HTTPS({ trustProtoHeader: true }));
}

require('_/twitter')(app);

// views is directory for all template files
app.set('views', __dirname + '/../../views');
app.set('view engine', 'ejs');

app.use (require('./routes.js'));

module.exports = app;
