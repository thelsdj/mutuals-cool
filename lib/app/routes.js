var express = require('express');
var router = express.Router();
var controller = require('./controllers');

router.get('/', controller.index);
router.get('/:screen_name', controller.screen_name);

module.exports = router;
