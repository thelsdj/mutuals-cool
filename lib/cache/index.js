var cacheManager = require('cache-manager');
var redisStore = require('cache-manager-redis');
var config = require('_/config');

var redisCache = cacheManager.caching({
    store: redisStore,
    url: config.REDIS_URL
});

module.exports = redisCache;
