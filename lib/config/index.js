var merge = require('merge');
require('dotenv').config();

var env = process.env.NODE_ENV || 'development';

var cfg = merge(true, process.env);
cfg = merge.recursive(true, process.env, require('./env/'+env));

cfg.env = env;

module.exports = cfg;
