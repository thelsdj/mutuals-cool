var config = require('_/config');
var redis = require('redis');
var redisConnect = require('connect-redis');

module.exports = function(session) {
    var module = redis.createClient(config.REDIS_URL);
    var RedisStore = redisConnect(session);

    module.getStore = function() {
        return new RedisStore({client: module});
    };

    return module;
};
