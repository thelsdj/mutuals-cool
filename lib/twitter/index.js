var Flutter = require('flutter');
var Twitter = require('twitter');
var config = require('_/config');
var log = require('_/log');
var redis = require('_/redis');
var cache = require('_/cache');

function getTwitterClient(request) {
    var twitter;
    if (request) {
        twitter = new Twitter({
            consumer_key: config.TWITTER_CONSUMER_KEY,
            consumer_secret: config.TWITTER_CONSUMER_SECRET,
            access_token_key: request.session.oauthAccessToken,
            access_token_secret: request.session.oauthAccessTokenSecret
        });
    }
    else {
        twitter = new Twitter({
            consumer_key: config.TWITTER_CONSUMER_KEY,
            consumer_secret: config.TWITTER_CONSUMER_SECRET,
            bearer_token: config.TWITTER_BEARER_TOKEN
        });
    }

    twitter.uncachedGet = twitter.get;
    function cachedGet(path, params, callback) {
        var key = path + JSON.stringify(params);
        var ttl = 10 * 60; /* 10 minutes */
        cache.wrap(key, function(cacheCallback) {
            twitter.uncachedGet(path, params, cacheCallback);
        }, {ttl: ttl}, callback);
    }

    twitter.get = cachedGet;

    return twitter;
}

var flutter = new Flutter({
    debug: log.info,
    consumerKey: config.TWITTER_CONSUMER_KEY,
    consumerSecret: config.TWITTER_CONSUMER_SECRET,
    loginCallback: config.TWITTER_LOGIN_CALLBACK,

    cacheClient: redis,

    authCallback: function (request, response) {
        if (request.error) {
            response.status(500).send(request.error);
        }
        else {
            var twitter = getTwitterClient(request);
            twitter.get('account/verify_credentials',
                { include_entities: false, skip_status: true },
                function (error, result) {
                    if (error)
                        response.status(500).send(error);
                    else {
                        response.redirect('/' + result.screen_name);
                    }
                }
            );

        }
    }
});

module.exports = function(app) {
    app.get('/twitter/connect', flutter.connect);
    app.get('/twitter/callback', flutter.auth);

    app.use(function(request, response, next) {
        var twitter = getTwitterClient(request);
        twitter.get('account/verify_credentials',
            { include_entities: false, skip_status: true },
            function (error, result) {
                response.locals.creds = false;
                if (error) {
                    switch(error[0].code) {
                        case 89:
                        case 220: /* Your credentials do not allow access to
                                        this resource */
                            /* ignore these */
                            break;
                        default:
                            response.status(500).send(error);
                            break;
                    }
                } else {
                    response.locals.creds = result;
                }
                next();
            }
        );
    });

    var module = {};
    module.getClient = getTwitterClient;

    return module;
};
